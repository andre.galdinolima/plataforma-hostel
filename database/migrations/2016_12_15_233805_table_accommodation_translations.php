<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAccommodationTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('language');
            $table->string('title');
            $table->string('subtitle');
            $table->text('description');
            $table->text('observations');
            $table->integer('accommodation_id')->unsigned();
            $table->foreign('accommodation_id')
                    ->references('id')
                    ->on('accommodations')
                    ->onDelete('cascade');

            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accommodation_translations');
    }
}
