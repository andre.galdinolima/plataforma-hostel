<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('rss');
            $table->string('whatsapp')->nullable();
            $table->string('facebook');
            $table->string('instagram');
            $table->string('twitter');
            $table->string('gplus');
            $table->string('cep');
            $table->string('cidade');
            $table->string('estado');
            $table->string('pais');
            $table->string('endereco');
            $table->string('numero');
            $table->string('bairro');
            $table->string('praia');
            $table->string('telefone');
            $table->string('email');
            $table->string('atendimento');
            $table->timestamps();

            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configurations');
    }
}
