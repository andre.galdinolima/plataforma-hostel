<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Type;
use App\TypeTranslation;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$type_accommodation = new Type;

        $languages = array();
        $type_trans_pt = new TypeTranslation;
    	$type_trans_pt->name = "Compartilhado";
    	$type_trans_pt->description = "Compartilhado com outros viajantes";
        $type_trans_pt->language = 'pt';
        $languages[]=$type_trans_pt;

        $faker_en = Faker::create();
        $type_trans_en = new TypeTranslation;
    	$type_trans_en->name = "Shared";
    	$type_trans_en->description = "shared with others travelers";
        $type_trans_en->language = 'en';
        $languages[]=$type_trans_en;

 		$faker_es = Faker::create('es_ES');
        $type_trans_es = new TypeTranslation;
    	$type_trans_es->name = "Compartido";
    	$type_trans_es->description = "Comparthado com otros viajeros";
        $type_trans_es->language = 'es';
        $languages[]=$type_trans_es;


        $type_accommodation->save();
        $type_accommodation->translations()->saveMany($languages); 


        $type_accommodation1 = new Type;

        $languages1 = array();
        $type_trans_pt1 = new TypeTranslation;
        $type_trans_pt1->name = "Privado";
        $type_trans_pt1->description = "Seu proprio quarto";
        $type_trans_pt1->language = 'pt';
        $languages1[]=$type_trans_pt1;

        $faker_en = Faker::create();
        $type_trans_en1 = new TypeTranslation;
        $type_trans_en1->name = "Private";
        $type_trans_en1->description = "only your room";
        $type_trans_en1->language = 'en';
        $languages1[]=$type_trans_en1;

        $faker_es = Faker::create('es_ES');
        $type_trans_es1 = new TypeTranslation;
        $type_trans_es1->name = "Privado";
        $type_trans_es1->description = "Solo tu habitación";
        $type_trans_es1->language = 'es';
        $languages1[]=$type_trans_es1;


        $type_accommodation1->save();
        $type_accommodation1->translations()->saveMany($languages1);
    }
}
