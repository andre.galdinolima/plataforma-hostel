<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Service::create([
    		'name'=>'Wifi',
    		'icon'=>'wifi-access.svg'
    	]);

    	Service::create([
    		'name'=>'Translado',
    		'icon'=>'frontal-taxi.svg'
    	]);

    	Service::create([
    		'name'=>'Café da manhã',
    		'icon'=>'soup-spoon-and-fork.svg'
    	]);

    	Service::create([
    		'name'=>'Recepção 24H',
    		'icon'=>'home-telephone.svg'
    	]);

    	Service::create([
    		'name'=>'Lavanderia',
    		'icon'=>'laundry-service.svg'
    	]);

    	Service::create([
    		'name'=>'Diversão',
    		'icon'=>'karaoke-room.svg'
    	]);
    }
}
