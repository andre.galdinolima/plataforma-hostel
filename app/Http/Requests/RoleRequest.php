<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'name'       => 'required|min:3',
                    'roles'      => 'required'
                ];
                break;
            }
            case 'PUT':
            return [
                    'role_id'        =>  'required|exists:roles,id',
                    'name'       => 'required|min:3'
                ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }

    }
}
