<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
   /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'title'       => 'required|min:3',
                    'body'       => 'required',
                    'pic'       =>  'mimes:jpeg,bmp,png',
                    'youtube'       => 'min:3',
                    'gallery_id' => 'exists:galleries,id',
                    'category_id' => 'required|exists:categories,id'
                ];
                break;
            }
            case 'PUT':
                return [
                        'post_id' => 'required|exists:posts,id',
                        'title'       => 'required|min:3',
                        'body'       => 'required',
                        'pic'       =>  'mimes:jpeg,bmp,png',
                        'youtube'       => 'min:3',
                        'gallery_id' => 'exists:galleries,id',
                        'category_id' => 'required|exists:categories,id'
                    ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }
}
