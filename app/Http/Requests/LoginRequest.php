<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'email'       => 'required|email|exists:users,email',
                    'password'       => 'required'
                ];
                break;
            }
            case 'PUT':
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }

    /*public function messages()
    {
        return [
            'email.required' => 'O campo email é obrigatorio.',
            'email.email' => 'O campo email deve não é um email valido',
            'email.exists' => 'O campo email informado não existe',

            'password.required' => 'O campo password é obrigatorio.',
        ];

    }
    */
}
