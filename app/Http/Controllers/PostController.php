<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use App\Category;
use App\Gallery;
use Sentinel;
use File;

class PostController extends HostelController
{
    public function index()
    {
        if (!Sentinel::getUser()->hasAccess('post.index')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }

    	$posts = Post::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.post.index',compact('posts'));
    }

    public function create()
    {
        if (!Sentinel::getUser()->hasAccess('post.create')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$categories = Category::all();
        $galleries = Gallery::all();
    	return view('admin.post.create',compact('categories','galleries'));
    }

    public function store(PostRequest $request)
    {
    	try {
            if (!Sentinel::getUser()->hasAccess('post.create')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }

    		$post = new Post;
	    	$post->title = $request->get('title');
	    	$post->body = $request->get('body');
            if ($request->has('youtube')) {
                $post->youtube = $request->get('youtube');
            }

            if ($request->has('gallery_id')) {
                $gallery = Gallery::findOrFail($request->get('gallery_id'));
                $post->gallery()->associate($gallery);
            }
	    	$post->user()->associate(Sentinel::getUser());
	    	$category = Category::findOrFail($request->get('category_id'));
	    	$post->category()->associate($category);

            if ($file = $request->file('pic')) {
                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '/uploads/poi/';
                $destinationPath = public_path() . $folderName;
                $safeName = md5($post->title) . '.' . $extension;
                $file->move($destinationPath, $safeName);
                $post->pic = $safeName;
            }

	    	$post->save();	
	    	return redirect()->route('dash.post.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}    	
    }

    public function edit(Post $post)
    {
        if (!Sentinel::getUser()->hasAccess('post.edit')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$categories = Category::all();
        $galleries = Gallery::all();
    	return view('admin.post.edit',compact('categories','post','galleries'));
    }

    public function update(PostRequest $request, Post $post)
    {
    	try {

            if (!Sentinel::getUser()->hasAccess('post.edit')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }

    		$post->title = $request->get('title');
	    	$post->body = $request->get('body');
	    	$category = Category::findOrFail($request->get('category_id'));
	    	$post->category()->associate($category);

            if ($request->has('youtube')) {
                $post->youtube = $request->get('youtube');
            }

            if ($request->has('gallery_id')) {
                $gallery = Gallery::findOrFail($request->get('gallery_id'));
                $post->gallery()->associate($gallery);
            }

            if ($file = $request->file('pic')) {
                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = '/uploads/poi/';
                $destinationPath = public_path() . $folderName;
                $safeName = md5($post->title) . '.' . $extension;
                $file->move($destinationPath, $safeName);
                
                if (File::exists(public_path() . $folderName . $post->pic)) {
                    File::delete(public_path() . $folderName . $post->pic);
                }
                $post->pic = $safeName;
            }

	    	$post->save();	
	    	return redirect()->route('dash.post.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    }

    public function show(Post $post)
    {
        return view('admin.post.show',compact('post'));
    }


    public function destroy(Post $post)
    {
        try {
            if (!Sentinel::getUser()->hasAccess('post.delete')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
            $post->delete();
            return redirect()->route('dash.post.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
        }
    }
}
