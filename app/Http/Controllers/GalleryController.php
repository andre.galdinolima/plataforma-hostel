<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Media;
use App\Event;
use Sentinel;
use File;

class GalleryController extends HostelController
{
	public function index()
	{
		if (!Sentinel::getUser()->hasAccess('gallery.index')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
		$galleries = Gallery::orderBy('created_at', 'desc')->paginate(10);
		return view('admin.gallery.index',compact('galleries'));
	}

	public function create()
	{
		if (!Sentinel::getUser()->hasAccess('gallery.create')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
		return view('admin.gallery.create');
	}

	public function store(Request $request)
	{
		try {

			if (!Sentinel::getUser()->hasAccess('gallery.create')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
			$gallery = new Gallery;
			$gallery->title = $request->get('title');
			$gallery->description = $request->get('description');
			$gallery->user()->associate(Sentinel::getUser());
			$medias = array();
			$imgtitle = $request->get('titleimg');
			if ($request->hasFile('files')) {
				$files = $request->file('files');
				foreach ($files as $key => $file) {
					$media = new Media;
				
					$fileName = $file->getClientOriginalName();
					$extension = $file->getClientOriginalExtension() ?: 'png';
					$folderName = '/uploads/gallery/';
					$destinationPath = public_path() . $folderName;
					$safeName = md5($fileName) . time() . '.' . $extension;
					$newfile = $file->move($destinationPath, $safeName);

					list($width, $height) = getimagesize($newfile);
					$media->width=$width;
					$media->height=$height;
					$media->pic = $safeName;
					if (isset($imgtitle[$key]) || array_key_exists($imgtitle[$key])){
						$media->title = $imgtitle[$key];
					}
					$media->user()->associate(Sentinel::getUser());
					$medias[]=$media;
				}
			}
			$gallery->save();
			$gallery->photos()->saveMany($medias);
			return redirect()->route('dash.gallery.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}

	public function edit(Gallery $gallery)
	{
		if (!Sentinel::getUser()->hasAccess('gallery.edit')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
		return view('admin.gallery.edit',compact('gallery'));
	}

	public function update(Request $request, Gallery $gallery)
	{

		try {

			if (!Sentinel::getUser()->hasAccess('gallery.edit')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
			$gallery->title = $request->get('title');
			$gallery->description = $request->get('description');
			$medias = array();
			$old_files = json_decode($request->get('old_files'));
			$old_ids = array();
			$new_img = array();
			$folderName = '/uploads/gallery/';

			if(count($old_files)>0){
				foreach ($old_files as $key => $value) {
					if ($value->image != "") {
						$photo = $gallery->photos()->where('id',$value->id)->first();
						if($photo){
							$photo->title = $value->title;
							$photo->save();
							$old_ids[]=$photo->id;	
						}
					}else{
						$new_img[]=$value;
					}
				}
			}
			//$imgtitle = $request->get('titleimg');
			if ($request->hasFile('files')) {
				$files = $request->file('files');
				foreach ($files as $key => $file) {
					$media = new Media;
				
					$fileName = $file->getClientOriginalName();
					$extension = $file->getClientOriginalExtension() ?: 'png';
					
					$destinationPath = public_path() . $folderName;
					$safeName = md5($fileName) . time() . '.' . $extension;
					$newfile = $file->move($destinationPath, $safeName);

					list($width, $height) = getimagesize($newfile);
					$media->width=$width;
					$media->height=$height;
					$media->pic = $safeName;

					foreach ($new_img as $key => $value) {
						if($value->image == $fileName){
							$media->title = $value->title;	
						}
					}
					$media->user()->associate(Sentinel::getUser());
					$medias[]=$media;
				}
			}

			$gallery->save();
			if(count($old_ids)>0){
				foreach ($gallery->photos()->whereNotIn('id',$old_ids)->get() as $value) {
					if (File::exists(public_path() . $folderName . $value->pic)) {
	    				File::delete(public_path() . $folderName . $value->pic);
	    				$value->delete();
	    			}
				}
			}
			
			

			//$gallery->photos()->delete();
			$gallery->photos()->saveMany($medias);
			return redirect()->route('dash.gallery.edit',$gallery->id);
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}

	public function show(Gallery $gallery)
	{
		return view('admin.gallery.show',compact('gallery'));
	}

	public function destroy(Gallery $gallery)
	{
		try {
			if (!Sentinel::getUser()->hasAccess('gallery.delete')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
			$gallery->delete();
			return redirect()->route('dash.gallery.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}
}
