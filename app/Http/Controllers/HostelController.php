<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration;
use Session;
use Illuminate\Support\Facades\View;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Type;
use App\Category;

class HostelController extends Controller
{
	protected $hostel;
    public function __construct()
    {
    	if (Session::has('hostel')) {
    		$this->hostel = Session::get('hostel');
    	}else{
			$this->hostel = Configuration::find(1);	
    	}
        $types = Type::all();
        $categories = Category::all();
    	view::share('configuration',$this->hostel);
        view::share('type_accommocations',$types);
        view::share('menu_categories',$categories);
    }
}
