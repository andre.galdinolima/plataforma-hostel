<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeRequest;
use App\Type;
use App\TypeTranslation;
use Illuminate\Support\Facades\App;
use Sentinel;

class TypeController extends Controller
{
    public function index()
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.index')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$type_accommodations = Type::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.type_accommodation.index',compact('type_accommodations'));
    }

    public function create()
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.create')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	return view('admin.type_accommodation.create');
    }

    public function store(TypeRequest $request)
    {
    	try {
            if (!Sentinel::getUser()->hasAccess('accommodation.create')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
    		$type_accommodation = new Type;

            $languages = array();
            $type_trans_pt = new TypeTranslation;
	    	$type_trans_pt->name = $request->get('portuguese_name');
	    	$type_trans_pt->description = $request->get('portuguese_description');
            $type_trans_pt->language = 'pt';
            $languages[]=$type_trans_pt;

            $type_trans_en = new TypeTranslation;
            $type_trans_en->name = $request->get('english_name');
            $type_trans_en->description = $request->get('english_description');
            $type_trans_en->language = 'en';
            $languages[]=$type_trans_en;

            $type_trans_es = new TypeTranslation;
            $type_trans_es->name = $request->get('spanish_name');
            $type_trans_es->description = $request->get('spanish_description');
            $type_trans_es->language = 'es';
            $languages[]=$type_trans_es;


            $type_accommodation->save();
            $type_accommodation->translations()->saveMany($languages); 

	    	return redirect()->route('dash.type_accommodation.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    	
    	
    }

    public function edit(Type $type_accommodation)
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.edit')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	return view('admin.type_accommodation.edit',compact('type_accommodation'));
    }

    public function update(TypeRequest $request, Type $type_accommodation)
    {
    	try {    		
            if (!Sentinel::getUser()->hasAccess('accommodation.edit')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
            $type_trans_pt = TypeTranslation::findOrFail($request->get('pt_type_translation_id'));
            $type_trans_pt->name = $request->get('portuguese_name');
            $type_trans_pt->description = $request->get('portuguese_description');
            $type_trans_pt->save();  

            $type_trans_en = TypeTranslation::findOrFail($request->get('en_type_translation_id'));
            $type_trans_en->name = $request->get('english_name');
            $type_trans_en->description = $request->get('english_description');
            $type_trans_en->save(); 

            $type_trans_es = TypeTranslation::findOrFail($request->get('es_type_translation_id'));
            $type_trans_es->name = $request->get('spanish_name');
            $type_trans_es->description = $request->get('spanish_description');
	    	$type_trans_es->save();	

	    	return redirect()->route('dash.type_accommodation.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    }

    public function show(Type $type_accommodation)
    {
        return view('admin.type_accommodation.show',compact('type_accommodation'));
    }


    public function destroy(Type $type_accommodation)
    {
        try {
            if (!Sentinel::getUser()->hasAccess('accommodation.delete')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
            $type_accommodation->delete();
            return redirect()->route('dash.type_accommodation.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
        }
    }
}
