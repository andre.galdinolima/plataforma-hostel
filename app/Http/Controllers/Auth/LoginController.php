<?php

namespace App\Http\Controllers\Auth;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Exception;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\HostelController;
use Sentinel;
use App\User;


class LoginController extends HostelController
{
   /**
     * Account sign in.
     *
     * @return View
     */
    public function getSignin()
    {
        return View('admin.login');
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postSignin(LoginRequest $request)
    {
        $error="";
        $dados = $request->only('email','password');
        try {
            if(Sentinel::authenticate($dados, false))
            {
                //Toastr::success('Usuario logado com sucesso');
                return redirect()->intended();
            }

            throw new Exception("Usuario ou Senha não conferem");
            

        } catch (NotActivatedException $e) {
            $error = $e->getMessage();
        } catch (ThrottlingException $e2) {
            $error = $e2->getMessage();
        } catch (Exception $e3){
            $error = $e3->getMessage();
        }
        //Toastr::error($error);
        return redirect()->back();
    }


     /**
     * Logout page.
     *
     * @return Redirect
     */
    public function getLogout()
    {
        Sentinel::logout();
        //Toastr::success('Logout com sucesso');
        return redirect('/');
    }
}
