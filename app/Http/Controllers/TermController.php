<?php

namespace App\Http\Controllers;

use App\Term;
use App\TermTranslation;
use App\Http\Requests\TermRequest;
use App\Icon;
class TermController extends Controller
{
    public function index()
    {
    	$terms = Term::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.terms.index',compact('terms'));
    }

    public function create()
    {
        $files = Icon::all();
    	return view('admin.terms.create',compact('files'));
    }

    public function store(TermRequest $request)
    {
    	try {
    		$term = new Term;
	    	$term->icon = $request->get('icon');

            $languages = array();
            $trans_pt = new TermTranslation;
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new TermTranslation;
            $trans_en->title = $request->get('english_title');
            $trans_en->description = $request->get('english_description');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new TermTranslation;
            $trans_es->title = $request->get('spanish_title');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->language = 'es';
            $languages[]=$trans_es;
            
            $term->save();   
            $term->translations()->saveMany($languages);
	    	
	    	return redirect()->route('dash.term.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    	
    	
    }

    public function edit(Term $term)
    {
        $files = Icon::all();
    	return view('admin.terms.edit',compact('term','files'));
    }

    public function update(TermRequest $request, Term $term)
    {
    	try {
	    	$term->icon = $request->get('icon');

			$languages = array();
            $trans_pt = new TermTranslation;
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new TermTranslation;
            $trans_en->title = $request->get('english_title');
            $trans_en->description = $request->get('english_description');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new TermTranslation;
            $trans_es->title = $request->get('spanish_title');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->language = 'es';
            $languages[]=$trans_es;
            

            $term->save();
            $term->translations()->saveMany($languages);
	    		
	    	return redirect()->route('dash.term.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    }

    public function show(Term $term)
    {
        return view('admin.terms.show',compact('term'));
    }


    public function destroy(Term $term)
    {
        try {
            $term->delete();
            return redirect()->route('dash.term.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
        }
    }
}
