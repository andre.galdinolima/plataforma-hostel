<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\Traits\LangTrait;

class Service extends Model
{
    use LangTrait;
    
    protected $fillable = ['icon'];
    protected $langOrderBy = 'service_id';
    
    public function translations()
    {
        return $this->hasMany('App\ServiceTranslation');
    }

    // public function translation($language = null)
    // {
    //     if ($language == null) {
    //         $language = App::getLocale();
    //     }
    //     return $this->translations()->where('language', '=', $language)->orderBy('service_id','asc')->first();
    // }
}
