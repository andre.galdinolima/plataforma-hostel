<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $fillable = ['language', 'name', 'category_id'];
    protected $table = 'category_translations';
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
