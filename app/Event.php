<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Support\Facades\App;
use App\Traits\LangTrait;

class Event extends Model
{
    use SoftDeletes,Sluggable,SluggableScopeHelpers,LangTrait;

    protected $guarded = ['id'];
    protected $dates = ['deleted_at','start_date'];
    protected $langOrderBy = 'event_id';


    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function gallery()
    {
    	return $this->belongsTo('App\Gallery')->withTrashed();
    }

    public function translations()
    {
        return $this->hasMany('App\EventTranslation');
    }

    // public function translation($language = null)
    // {
    //     if ($language == null) {
    //         $language = App::getLocale();
    //     }
    //     return $this->translations()->where('language', '=', $language)->orderBy('event_id','asc')->first();
    // }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
