<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Support\Facades\App;

class Category extends Model
{
	use Sluggable,SoftDeletes,SluggableScopeHelpers;
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    public function parent()
    {
    	return $this->belongsTo('App\Category','parent_id');
    }

    public function childrens()
    {
    	return $this->hasMany('App\Category','parent_id');
    }


    public function posts()
    {
        return $this->hasMany('App\Post')->withTrashed();
    }

    public function translations()
    {
        return $this->hasMany('App\CategoryTranslation');
    }

    public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }
        return $this->translations()->where('language', '=', $language)->orderBy('category_id','asc')->first();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
