<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTranslation extends Model
{
    protected $table = 'service_translations';
    protected $fillable = ['language', 'name', 'service_id'];

    public function service()
    {
        return $this->belongsTo('App\Term');
    }
}
