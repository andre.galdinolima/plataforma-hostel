<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	protected $table = 'medias';
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function gallery()
    {
    	return $this->belongsTo('App\Gallery')->withTrashed();
    }
    
}
