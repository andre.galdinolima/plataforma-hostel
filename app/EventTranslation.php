<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EventTranslation extends Model
{
    protected $fillable = ['language', 'title', 'description','event_id'];
    protected $table = 'event_translations';
    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
