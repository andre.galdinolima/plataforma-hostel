<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\Traits\LangTrait;

class Type extends Model
{
    use LangTrait;

    protected $langOrderBy = 'type_id';

    public function accommodations()
    {
    	return $this->hasMany('App\Accommodation')->withTrashed();
    }

    public function accommodationsActive()
    {
        return $this->hasMany('App\Accommodation');
    }

    // public function translation($language = null)
    // {
    //     if ($language == null) {
    //         $language = App::getLocale();
    //     }
    //     return $this->translations()->where('language', '=', $language)->orderBy('type_id','asc')->first();
    // }

    public function translations()
    {
        return $this->hasMany('App\TypeTranslation');
    }
}
