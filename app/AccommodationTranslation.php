<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationTranslation extends Model
{
	
    protected $fillable = ['language', 'title', 'subtitle', 'description','observations','accommodation_id'];

    public function accommodation()
    {
        return $this->belongsTo('App\Accommodation');
    }
}
