@extends('layouts/default')


@section('title')
{{$post->title}}
@parent
@stop


@section('css')
<link rel="stylesheet" href="{{ asset('css/blog.css') }}">
@endsection

@section('body')

<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('blog')}}" class="breadcrumb grey-text">{{trans('menus.blog')}}</a>
					<a href="{{route('blog.show',$post->slug)}}" class="breadcrumb grey-text text-darken-3">{{$post->title}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col s12">
				@if($post->pic)
				<div class="cover">
					<img src="/uploads/poi/{{$post->pic}}" alt="">
				</div>
				@endif

				<a href="{{route('blog.show',$post->slug)}}">
					<h5 class="grey-text text-darken-3 center">{{$post->title}}</h5>
				</a>
				<div class="left">
					<h6 class="grey-text text-darken-1 light"><i class="material-icons left">access_time</i>{{$post->created_at->diffForHumans()}} by <a href="#">{{$post->user->name}}</a></h6>
				</div>
				<div class="right">
					<div class="chip">{{$post->category->name}}</div>
				</div>
				<br>
				<br>
				<div class="divider"></div>

				<div class="post">
					{!!html_entity_decode($post->body)!!}
				</div>

				<div class="divider"></div>
			</div> 
		</div>
	</div>
</div>
@endsection

@section('script')
@endsection