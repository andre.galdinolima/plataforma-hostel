@extends('layouts/default')


@section('title')
{{trans('menus.terms')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/terms.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('terms')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.terms')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<h4 class="grey-text text-darken-3 center">{{ trans('messages.terms') }}</h4>
		<div class="row center">
			@forelse($terms as $term)
			<div class="col s12 m4">
				<div class="item">
					<img src="/images/icons/{{$term->icon}}" class="icon">
					<h5 class="grey-text text-darken-3 center">{{$term->translation()->title}}</h5>
					<h6 class="grey-text text-darken-1 light center justify">{!!html_entity_decode($term->translation()->description)!!}</h6>
				</div>
			</div>
			@empty
			@endforelse
		</div>
	</div>
</div>
@endsection

@section('script')
<!-- <script src="{{ asset('js/terms.js') }}"></script> -->
@endsection