@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Usuários</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.user.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>
	
	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="email">Email</th>
							<th data-field="lastlogin">Ultimo Login</th>
							<th data-field="permission">Permissão</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($users as $user)
						<tr>
							<td>{{$user->name}}</td>
							<td>{{$user->email}}</td>
							<td>{{$user->last_login}}</td>
							<td>
								@if(isset($user->roles[0]))
								{{ $user->roles[0]->name }}
								@else
								Sem Grupo
								@endif
							</td>
							<td class="right">
								<a href="{{ route('dash.user.edit',$user->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
								@if(Sentinel::getUser()->id != $user->id)
								<a href="{{ route('dash.user.delete',$user->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								@endif

							</td>
						</tr>
						@empty
						<tr>
							<td colspan="6">
								No else users
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$users->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection