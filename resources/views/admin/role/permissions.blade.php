<table class="striped bordered">
	<thead>
		<tr>
			<th>Funcionalidade</th>
			<th>
				<input type="checkbox" id="create" class="check_all" />
				<label for="create" class="black-text">Criar</label>
			</th>
			<th>
				<input type="checkbox" id="edit" class="check_all"/>
				<label for="edit" class="black-text">Editar</label>
			</th>
			<th>
				<input type="checkbox" id="delete" class="check_all"/>
				<label for="delete" class="black-text">Apagar</label>
			</th>
			<th>
				<input type="checkbox" id="index" class="check_all"/>
				<label for="index" class="black-text">Listar</label>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Postagens</td>
			<td>				
				<input type="checkbox" id="create_post" name="roles[post][create]" 
				@if(isset($role))
				 	@if(array_key_exists('post.create',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="create_post" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="edit_post" name="roles[post][edit]" 
				@if(isset($role))
				 	@if(array_key_exists('post.edit',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="edit_post" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="delete_post" name="roles[post][delete]" 
				@if(isset($role))
				 	@if(array_key_exists('post.delete',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="delete_post" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="list_post" name="roles[post][index]"  
				@if(isset($role))
				 	@if(array_key_exists('post.index',$permissions))
				 		checked
				 	@endif 
				@endif
				 />
				<label for="list_post" class="black-text"></label>
			</td>
		</tr>
		<tr>
			<td>Eventos</td>
			<td>				
				<input type="checkbox" id="create_event" name="roles[event][create]" 
				@if(isset($role))
				 	@if(array_key_exists('event.create',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="create_event" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="edit_event" name="roles[event][edit]" 
				@if(isset($role))
				 	@if(array_key_exists('event.edit',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="edit_event" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="delete_event" name="roles[event][delete]"
				@if(isset($role))
				 	@if(array_key_exists('event.delete',$permissions))
				 		checked 
				 	@endif 
				@endif
				 />
				<label for="delete_event" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="list_event" name="roles[event][index]" 
				@if(isset($role))
				 	@if(array_key_exists('event.index',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="list_event" class="black-text"></label>
			</td>
		</tr>
		<tr>
			<td>Galeria de fotos</td>
			<td>				
				<input type="checkbox" id="create_gallery" name="roles[gallery][create]"
				@if(isset($role))
				 	@if(array_key_exists('gallery.create',$permissions))
				 		checked 
				 	@endif 
				@endif
				 />
				<label for="create_gallery" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="edit_gallery" name="roles[gallery][edit]" 
				@if(isset($role))
				 	@if(array_key_exists('gallery.edit',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="edit_gallery" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="delete_gallery" name="roles[gallery][delete]" 
				@if(isset($role))
				 	@if(array_key_exists('gallery.delete',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="delete_gallery" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="list_gallery" name="roles[gallery][index]"
				@if(isset($role))
				 	@if(array_key_exists('gallery.index',$permissions))
				 		checked 
				 	@endif 
				@endif
				 />
				<label for="list_gallery" class="black-text"></label>
			</td>
		</tr>
		<tr>
			<td>Acomodações</td>
			<td>				
				<input type="checkbox" id="create_accommodation" name="roles[accommodation][create]"
				@if(isset($role))
				 	@if(array_key_exists('accommodation.create',$permissions))
				 		checked 
				 	@endif 
				@endif
				 />
				<label for="create_accommodation" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="edit_accommodation" name="roles[accommodation][edit]" 
				@if(isset($role))
				 	@if(array_key_exists('accommodation.edit',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="edit_accommodation" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="delete_accommodation" name="roles[accommodation][delete]" 
				@if(isset($role))
				 	@if(array_key_exists('accommodation.delete',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="delete_accommodation" class="black-text"></label>
			</td>
			<td>
				<input type="checkbox" id="list_accommodation" name="roles[accommodation][index]" 
				@if(isset($role))
				 	@if(array_key_exists('accommodation.index',$permissions))
				 		checked 
				 	@endif 
				@endif
				/>
				<label for="list_accommodation" class="black-text"></label>
			</td>
		</tr>
	</tbody>
</table>