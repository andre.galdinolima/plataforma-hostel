@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar Permissão</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form method="post" action="{{ route('dash.role.update',$role->id) }}" class="col s12">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<input type="hidden" name="role_id" value="{{$role->id}}">
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{$role->name}}">
						<label for="name">Nome</label>
					</div>
				</div>

				@include('admin/role/permissions')

				<br>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('js/roles.js') }}"></script>
@endsection