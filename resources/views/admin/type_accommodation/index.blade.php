@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Tipos de acomodação</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>
	
	<div class="fixed-action-btn">
	<a href="{{route('dash.type_accommodation.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="language">Language</th>
							<th data-field="language">Quantidade de quartos</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($type_accommodations as $type_accommodation)
						<tr>
							<td>{{$type_accommodation->translation()->name}}</td>
							<td>{{$type_accommodation->translation()->language}}</td>
							<td>{{$type_accommodation->accommodations->count()}}</td>
							<td class="right">
							@if (Sentinel::getUser()->hasAccess('accommodation.delete'))
								<a href="{{ route('dash.type_accommodation.delete',$type_accommodation->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
							@endif
							@if (Sentinel::getUser()->hasAccess('accommodation.edit'))
								<a href="{{ route('dash.type_accommodation.edit',$type_accommodation->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							@endif
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">
								No else type_accommodations
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$type_accommodations->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection