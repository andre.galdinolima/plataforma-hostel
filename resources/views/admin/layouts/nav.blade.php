<header>
	<nav class="orange darken-1">
		<div class="nav-wrapper">
			<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only">
				<i class="material-icons black-text">menu</i>
			</a>
			<a href="#" class="brand-logo right hide-on-large-only black-text">Canguru Hostel</a>
			<ul class="right hide-on-med-and-down">
				<li>
					<a href="{{ route('dash.config') }}">
						<i class="material-icons black-text">settings</i>
					</a>
				</li>
				<li>
					<a href="{{route('logout')}}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Sair">
						<i class="material-icons black-text">exit_to_app</i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<ul id="nav-mobile" class="side-nav custom-side-nav fixed collapsible grey darken-4" data-collapsible="accordion">
		<li class="grey darken-4">
			<div class="logo">
				<img src="/images/logo.svg" alt="Canguru Hostel">
			</div>
		</li>
		<li>
			<a class="waves-effect" href="{{route('dash.index')}}">
				<i class="material-icons">dashboard</i>
				Dashboard
			</a>
		</li>
		<li>
			<a class="waves-effect" href="{{route('index')}}">
				<i class="material-icons">home</i>
				Home
			</a>
		</li>
		<li>
			<a class="waves-effect" href="{{ route('dash.config') }}">
				<i class="material-icons">settings</i>
				Configurações
			</a>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">hotel</i>Acomodações</div>
			<div class="collapsible-body">
				<ul class="collapsible" data-collapsible="accordion">
					<li>
						<a href="{{ route('dash.accommodation.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.accommodation.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Nova
						</a>
					</li>
					<li>
						<div class="collapsible-header waves-effect"><i class="material-icons">hotel</i>Tipos de acomodação</div>
						<div class="collapsible-body">
							<ul>
								<li>
									<a href="{{ route('dash.type_accommodation.index') }}" class="waves-effect">
										<i class="material-icons">view_headline</i>
										Listar
									</a>
								</li>
								<li>
									<a href="{{ route('dash.type_accommodation.create') }}" class="waves-effect">
										<i class="material-icons">add</i>
										Nova
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="collapsible-header waves-effect"><i class="material-icons">notifications</i>Serviços</div>
						<div class="collapsible-body">
							<ul>
								<li>
									<a href="{{ route('dash.service.index') }}" class="waves-effect">
										<i class="material-icons">view_headline</i>
										Listar
									</a>
								</li>
								<li>
									<a href="{{ route('dash.service.create') }}" class="waves-effect">
										<i class="material-icons">add</i>
										Nova
									</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">event</i>Eventos</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.event.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.event.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">collections</i>Galeria de fotos</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.gallery.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.gallery.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Nova
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">mood</i>Ícones</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.icon.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.icon.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">lock</i>Permissões</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.role.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.role.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">description</i>Postagens</div>
			<div class="collapsible-body">
				<ul class="collapsible" data-collapsible="accordion">
					<li>
						<a href="{{ route('dash.post.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.post.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Nova
						</a>
					</li>
					<li>
						<div class="collapsible-header waves-effect"><i class="material-icons">book</i>Categorias</div>
						<div class="collapsible-body">
							<ul>
								<li>
									<a href="{{ route('dash.category.index') }}" class="waves-effect">
										<i class="material-icons">view_headline</i>
										Listar
									</a>
								</li>
								<li>
									<a href="{{ route('dash.category.create') }}" class="waves-effect">
										<i class="material-icons">add</i>
										Nova
									</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">people</i>Staff</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.staff.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.staff.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">assignment</i>Termos e condições</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.term.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.term.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<div class="collapsible-header waves-effect"><i class="material-icons">people_outline</i>Usuários</div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a href="{{ route('dash.user.index') }}" class="waves-effect">
							<i class="material-icons">view_headline</i>
							Listar
						</a>
					</li>
					<li>
						<a href="{{ route('dash.user.create') }}" class="waves-effect">
							<i class="material-icons">add</i>
							Novo
						</a>
					</li>
				</ul>
			</div>
		</li>
		<li>
			<a class="waves-effect" href="{{route('logout')}}">
				<i class="material-icons">exit_to_app</i>
				Sair
			</a>
		</li>
	</ul>
</header>