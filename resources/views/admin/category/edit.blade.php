@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar Categoria</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
		<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<form method="post" action="{{ route('dash.category.update', $category->id) }}" class="col s12">
			{{ csrf_field() }}
			{{ method_field('PUT')}}
			<input type="hidden" name="category_id" value="{{$category->id}}">
			<div id="portuguese">					
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="portuguese_name" value="{{ $category->translation('pt')->name }}">
					<label for="name">Nome</label>
					</div>
				</div>
			</div>
			<div id="english">					
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="english_name" value="{{ $category->translation('en')->name }}">
					<label for="name">Nome</label>
					</div>
				</div>
			</div>
			<div id="spanish">					
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="spanish_name" value="{{ $category->translation('es')->name }}">
					<label for="name">Nome</label>
					</div>
				</div>
			</div>

			<div class="row center">
				<div class="col s12">
					<button class="waves-effect btn black-text" type="submit">Enviar</button>
				</div>
			</div>
		</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection