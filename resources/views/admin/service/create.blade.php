@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Novo serviço</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<form class="col s12" method="post" action="{{ route('dash.service.store') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div id="portuguese" class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" name="portuguese_name" value="{{ old('portuguese_name') }}">
							<label for="name">Nome</label>
						</div>
					</div>
				</div>

				<div id="english" class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" name="english_name" value="{{ old('english_name') }}">
							<label for="name">Nome</label>
						</div>
					</div>
				</div>

				<div id="spanish" class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" name="spanish_name" value="{{ old('spanish_name') }}">
							<label for="name">Nome</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12">
						<div class="input-field col s12">
							<select class="icons" name="icon">
								<option value="" disabled selected>Escola um ícone para o serviço</option>
								@forelse($files as $file)
								<option class="left" value="{{$file->path}}" data-icon="/images/icons/{{$file->path}}">{{$file->name}}</option>
								@empty

								@endforelse
							</select>
						</div>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection