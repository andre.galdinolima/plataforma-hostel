@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar Evento</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>

			<form class="col s12" method="post" action="{{ route('dash.event.update',$event->id) }}"  enctype="multipart/form-data">
				{{ csrf_field() }}

				{{ method_field('PUT')}}
				<input type="hidden" name="event_id" value="{{$event->id}}">
				<div id="portuguese">					
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{ $event->translation('pt')->title }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="portuguese_description" name="portuguese_description">{{ $event->translation('pt')->description }}</textarea>
						</div>
					</div>
				</div>

				<div id="english">					
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{ $event->translation('en')->title }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="english_description" name="english_description">{{ $event->translation('en')->description }}</textarea>
						</div>
					</div>
				</div>

				<div id="spanish">					
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{ $event->translation('es')->title }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="spanish_description" name="spanish_description">{{ $event->translation('es')->description }}</textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="date" type="date" class="datepicker" name="start_date" value="{{ $event->start_date->format('d/m/Y') }}">
						<label class="active" for="date">Data de início</label>

					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
							@if(isset($event->gallery) && $event->gallery->id == $gallery->id)
							<option value="{{$gallery->id}}" selected>{{$gallery->title}}</option>
							@else
							<option value="{{$gallery->id}}">{{$gallery->title}}</option>
							@endif
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row">
					<div class="file-field input-field col s12">
						<div class="btn">
							<span class="black-text">Procurar</span>
							<input type="file" name="pic">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text" placeholder="Adicionar imagem para o evento" >
						</div>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Postar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/event.js') }}"></script>
@endsection