@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Termos e condições</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.term.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="title">Nome</th>
							<th data-field="icon">Icone</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($terms as $term)
						<tr>
							<td>{{$term->translation('pt')->title}}</td>
							<td> <img class="icon" src="{{ asset('images/icons/'.$term->icon) }}"></td>
							<td class="right">
								<a href="{{ route('dash.term.delete',$term->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								<a href="{{ route('dash.term.edit',$term->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">
								No else terms
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$terms->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection