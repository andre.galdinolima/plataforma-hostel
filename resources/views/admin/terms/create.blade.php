@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Nova regra</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<form class="col s12" method="post" action="{{ route('dash.term.store') }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div id="portuguese">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{ old('portuguese_title') }}">
							<label for="title">Nome</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea name="portuguese_description" id="portuguese_description" rows="10">{{ old('portuguese_description') }}</textarea>
						</div>
					</div>
				</div>

				<div id="english">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{ old('english_title') }}">
							<label for="title">Nome</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea name="english_description" id="english_description" rows="10">{{ old('english_description') }}</textarea>
						</div>
					</div>
				</div>

				<div id="spanish">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{ old('spanish_title') }}">
							<label for="title">Nome</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea name="spanish_description" id="spanish_description" rows="10">{{ old('spanish_description') }}</textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12">
						<div class="input-field col s12">
							<select class="icons" name="icon">
								<option value="" disabled selected>Escola um ícone para a regra</option>
								@forelse($files as $file)
								<option class="left" value="{{$file->path}}" data-icon="/images/icons/{{$file->path}}">{{$file->name}}</option>
								@empty
								@endforelse
							</select>
						</div>
					</div>
				</div>
				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/term.js') }}"></script>
@endsection