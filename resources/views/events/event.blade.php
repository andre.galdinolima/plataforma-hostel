@extends('layouts/default')


@section('title')
{{$event->title}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/event_item.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('events')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.event')}}</a>
					<a href="{{route('events.show',$event->slug)}}" class="breadcrumb grey-text">{{$event->translation()->title}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="event">			
					<img src="/uploads/events/{{$event->pic}}" title="{{$event->translation()->title}}">
					<h5 class="grey-text text-darken-3 center">{{$event->translation()->title}}</h5>
					<div class="left">
						<h6 class="grey-text text-darken-1 light"><i class="material-icons left">event</i>{{$event->start_date->format('d/m/Y')}}</h6>
					</div>
					<div class="right">
						<h6 class="grey-text text-darken-1 light">{{$configuration->name}}</h6>
					</div>
					<br>
					<br>
					<div class="divider"></div>

					<div class="description justify">
						{!!html_entity_decode(str_limit($event->translation()->description,200))!!}
					</div>

				</div>
				<div class="divider"></div>
			</div> 
		</div>
	</div>
</div>

@endsection

@section('script')
@endsection