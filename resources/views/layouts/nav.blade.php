<div class="navbar-top grey darken-4 hide-on-med-and-down">
	<ul class="left">
		@if($configuration->facebook)
		<li><a href="https://www.facebook.com/{{$configuration->facebook}}" class="white-text" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a></li>
		@endif
		@if($configuration->instagram)
		<li><a href="https://www.instagram.com/{{$configuration->instagram}}" class="white-text" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a></li>
		@endif
		@if($configuration->twitter)
		<li><a href="https://twitter.com/{{$configuration->twitter}}" class="white-text" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a></li>
		@endif
		@if($configuration->gplus)
		<li><a href="https://plus.google.com/{{$configuration->gplus}}" class="white-text" target="_blank"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a></li>
		@endif
		<li><a href="{{route('terms')}}" class="white-text">{{ trans('menus.terms') }}</a></li>
	</ul>
	<ul class="right">
		@foreach (Config::get('languages.locales') as $lang => $language)
		<li><a href="{{ route('lang.switch', $lang) }}" class="white-text"><img src="/images/{{ $language['icon'] }}" alt=""><span>{{ $language['language'] }}</span></a></li>
		@endforeach
	</ul>
</div>

<nav class="navbar-menu z-depth-0">
	<div class="nav-wrapper">
		<a href="{{route('index')}}" class="brand-logo"><img src="/images/logo.svg" alt="Canguru Hostel"></a>
		<a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
		<ul class="right hide-on-med-and-down nav-links">
			<li><a href="{{route('index')}}">Home</a></li>
			@if(count($type_accommocations) > 0)
			<li><a class="dropdown-button" data-activates="dropdown-rooms" data-beloworigin="true" data-beloworigin="false" data-constrainwidth="false">{{ trans('menus.rooms.rooms') }}<i class="material-icons right">arrow_drop_down</i></a></li>
			<ul id="dropdown-rooms" class="dropdown-content nested">
				@foreach($type_accommocations as $type)
				<li><a href="#" class="dropdown-button" data-activates="dropdown-rooms-{{$type->translation()->name}}" data-beloworigin="false"><span class="left">{{$type->translation()->name}}</span><i class="material-icons right">arrow_right</i></a></li>
				<ul id="dropdown-rooms-{{$type->translation()->name}}" class="dropdown-content">
					@forelse($type->accommodationsActive as $accommodation)
					<li><a href="{{route('room.accommodation',$accommodation->slug)}}">{{ $accommodation->translation()->title }}</a></li>
					@empty

					@endforelse
				</ul>
				@endforeach
			</ul>
			@endif
			<li><a href="{{route('galleries')}}">{{ trans('menus.photogallery') }}</a></li>
			<li><a href="{{route('events')}}">{{ trans('menus.event') }}</a></li>
			<li><a href="{{route('blog')}}">{{ trans('menus.blog') }}</a></li>
			<li><a class="dropdown-button" data-activates="dropdown-contact" data-beloworigin="true" data-beloworigin="false" data-constrainwidth="false">{{ trans('menus.contact') }}<i class="material-icons right">arrow_drop_down</i></a></li>
			<ul id="dropdown-contact" class="dropdown-content nested">
			<li><a href="{{route('location')}}" class="capitalize"><i class="fa fa-map left" aria-hidden="true"></i>{{ trans('menus.location') }}</a></li>
				@if($configuration->telefone)
				<li><a><i class="fa fa-phone left" aria-hidden="true"></i>{{$configuration->telefone}}</a></li>
				@endif
				@if($configuration->whatsapp)
				<li><a><i class="fa fa-whatsapp left" aria-hidden="true"></i>{{$configuration->whatsapp}}</a></li>
				@endif
				@if($configuration->email)
				<li><a href="mailto:{{$configuration->email}}" target="_top"><i class="fa fa-envelope left" aria-hidden="true"></i>{{$configuration->email}}</a></li>
				@endif
				@if($configuration->facebook)
				<li><a href="https://www.facebook.com/{{$configuration->facebook}}" target="_blank"><i class="fa fa-facebook left" aria-hidden="true"></i>fb.com/{{$configuration->facebook}}</a></li>
				@endif
				@if($configuration->instagram)
				<li><a href="https://www.instagram.com/{{$configuration->instagram}}" target="_blank"><i class="fa fa-instagram left" aria-hidden="true"></i>instagr.amd/{{$configuration->instagram}}</a></li>
				@endif
				@if($configuration->twitter)
				<li><a href="https://twitter.com/{{$configuration->twitter}}" target="_blank"><i class="fa fa-twitter left" aria-hidden="true"></i>twitter.com/{{$configuration->twitter}}</a></li>
				@endif
				@if($configuration->gplus)
				<li><a href="https://plus.google.com/{{$configuration->gplus}}" target="_blank" class="capitalize"><i class="fa fa-google-plus left" aria-hidden="true"></i>{{$configuration->name}}</a></li>
				@endif
			</ul>
			<a class="btn waves-effect black-text" href="{{route('booking')}}">{{ trans('menus.booknow') }}</a>
		</ul>
	</div>
</nav>

<ul class="side-nav nav-links collapsible" id="mobile-menu" data-collapsible="accordion">
	<li>
		<ul class="lang">
			@foreach (Config::get('languages.locales') as $lang => $language)
			<li><a href="{{ route('lang.switch', $lang) }}" class="white-text"><img src="/images/{{ $language['icon'] }}"></a></li>
			@endforeach
		</ul>
	</li>
	<li><a href="{{route('index')}}">Home</a></li>
	@if(count($type_accommocations) > 0)
	<li>
		<ul class="collapsible black-text" data-collapsible="accordion">
			<li>
				<div class="collapsible-header"><a href="#!">{{ trans('menus.rooms.rooms') }}<i class="material-icons right">arrow_drop_down</i></a></div>
				<div class="collapsible-body">
					<ul class="collapsible" data-collapsible="accordion">
						@foreach($type_accommocations as $type)
						<li>
							<div class="collapsible-header"><a href="#!">{{$type->translation()->name}}<i class="material-icons right">arrow_drop_down</i></a></div>
							<div class="collapsible-body">
								<ul class="collapsible" data-collapsible="accordion">
									@forelse($type->accommodations as $accommodation)
									<li>
										<div class="collapsible-header"><a href="{{route('room.accommodation',$accommodation->slug)}}">{{ $accommodation->translation()->title }}</a></div>
									</li>
									@empty
									@endforelse
								</ul>
							</div>
						</li>
						@endforeach
					</ul>
				</div>
			</li>
		</ul>
	</li>
	@endif
	<li><a href="{{route('galleries')}}">{{ trans('menus.photogallery') }}</a></li>
	<li><a href="{{route('events')}}">{{ trans('menus.event') }}</a></li>
	<li><a href="{{route('location')}}">{{ trans('menus.location') }}</a></li>
	<li><a href="{{route('blog')}}">{{ trans('menus.blog') }}</a></li>
	<a class="btn waves-effect black-text" href="{{route('booking')}}">{{ trans('menus.booknow') }}</a>
</ul>