<?php 

return [
	'detail' => 'Detalhes do Quarto',
	'obs' => 'Observações',
	'amenities' => 'Serviços',
	'bookings' => [
		'title' => 'Reservas',
		'rates' => "Você vai encontrar as melhores tarifas em nossa página de reserva",
		'bookingpage' => 'Pagina de reservas',
		'phone' => 'Telefone'
	],
	'photos' => 'Fotos'
];