<?php

return [
    'terms' => 'Terms and conditions',
    'deposit' => [
        'title' => '50% deposit',
        'message' => '50% of the total reservation cost is due at time of booking as a non-refundable deposit to guarantee your reservation. After your booking we will send you our bank account for the deposit. We also accept PayPal or credit card for long distance transactions.'
    ],
    'check' => [
        'title' => 'Check-in from 1 p.m. and check-out until 11 a.m.',
        'message' => 'Final payment is due at check-in. We will charge you for late check-out. The amount is up to one day rate to check-outs after 11 a.m.'
    ],
    'shuttle' => [
        'title' => 'Shuttles and rides',
        'message' => 'We offer free shuttle between the local bus station and the hostel for arrivals and departures. For other rides, remember to give a tip for the driver.'
    ],
    'breakfast' => [
        'title' => 'Breakfast included',
        'message' => 'Served from 8 a.m. to 10 a.m., our breakfast has a complete set of dishes with fruits, juices, tea, coffee, cakes, and a delicious freshly prepared sandwich by our chef.'
    ],
    'bar' => [
        'title' => 'Bar',
        'message' => 'It is not allowed to drink alcoholic beverages from outside the hostel inside it, but do not worry, our bar has the coldest beer and the best caipirinha in the city!'
    ],
    'bike' => [
        'title' => 'Bikes',
        'message' => 'We have some bikes to lend to our guests. Ask the staff for help to choose the best bike for you!'
    ],
    'bedsheet' => [
        'title' => 'Bed sheets',
        'message' => 'We provide bed sheets at check-in. Please, put them in the basked located at the reception at check-out.'
    ],
    'towel' => [
        'title' => 'Towels',
        'message' => 'In case you have forgotten yours, we rent towels for R$15, of which we refund R$10 (or we deduct from your check-out) when the towel is returned.'
    ],
    'laundry' => [
        'title' => 'Laundry',
        'message' => 'If you need to do your laundry, we have a R$40 fee for each cycle in our machine with capacity of 10kg.'
    ],
    'quiet' => [
        'title' => 'Quiet hours',
        'message' => 'Avoid bothering neighbors or other guests after 10 p.m.'
    ],
    'mosquito' => [
        'title' => 'Mosquitoes',
        'message' => "We are in a region with a high population of mosquitoes. Help us keeping the windows closed between 6 p.m. and 6 a.m.. We recommend you to bring your own repellent."
    ],
    'cancellation' => [
        'title' => 'Cancellation policy',
        'message' => 'No refunds for cancellations 7 days or less prior to arrival date. No refund for holiday bookings and commemorative dates.'
    ]
];
