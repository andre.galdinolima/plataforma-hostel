<?php 
return [
	'booknow' => 'Book now',
	'rooms' => [
		'rooms' => 'Rooms',
	],
	'photogallery' => 'Photo gallery',
	'event' => 'Events',
	'location' => 'Location',
	'contact' => 'Contact',
	'news' => 'News',
	'restrict' =>'Restrict access',
	'terms' =>'Terms and Conditions',
	'blog' => 'Attractions',
	'booking' => 'Booking',
	'404'	=>	'Page not found',
	'reset' =>  'Reset password'
];