$('#filer_input').filer({
  showThumbs: true,
  addMore: true,
  allowDuplicates: false,
  templates: {
    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
    item: '<li class="jFiler-item">\
    <div class="jFiler-item-container">\
    <div class="jFiler-item-inner">\
    <div class="jFiler-item-thumb">\
    <div class="jFiler-item-status"></div>\
    <div class="jFiler-item-thumb-overlay">\
    <div class="jFiler-item-info">\
    <div style="display:table-cell;vertical-align: middle;">\
    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    <span class="jFiler-item-others">{{fi-size2}}</span>\
    </div>\
    </div>\
    </div>\
    {{fi-image}}\
    </div>\
    <div class="jFiler-item-assets jFiler-row">\
    <ul class="list-inline pull-left">\
    <li><input placeholder="Título da imagem" id="title" name="titleimg[]" type="text" class="title"></li>\
    </ul>\
    <ul class="list-inline pull-right">\
    <li><a class="jFiler-item-trash-action"><i class="material-icons">delete</i></a></li>\
    </ul>\
    </div>\
    </div>\
    </div>\
    </li>',
    itemAppend: '<li class="jFiler-item">\
    <div class="jFiler-item-container">\
    <div class="jFiler-item-inner">\
    <div class="jFiler-item-thumb">\
    <div class="jFiler-item-status"></div>\
    <div class="jFiler-item-thumb-overlay">\
    <div class="jFiler-item-info">\
    <div style="display:table-cell;vertical-align: middle;">\
    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    <span class="jFiler-item-others">{{fi-size2}}</span>\
    </div>\
    </div>\
    </div>\
    {{fi-image}}\
    </div>\
    <div class="jFiler-item-assets jFiler-row">\
    <ul class="list-inline pull-left">\
    <li><input placeholder="Título da imagem" id="title" name="titleimg[]" type="text" class="title"></li>\
    </ul>\
    <ul class="list-inline pull-right">\
    <li><a class="jFiler-item-trash-action"><i class="material-icons">delete</i></a></li>\
    </ul>\
    </div>\
    </div>\
    </div>\
    </li>',
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: false,
    canvasImage: true,
    removeConfirmation: true,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.jFiler-item-trash-action'
    }
  },
  captions: {
    button: "Adicionar",
    feedback: "Escolha os arquivos para adicionar",
    feedback2: "arquivos selecionados",
    drop: "Drop file here to Upload",
    removeConfirmation: "Você tem certeza que deseja excluir esse arquivo?",
    errors: {
      filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
      filesType: "Only Images are allowed to be uploaded.",
      filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
      filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
    }
  }
});

var files = [];

var gallery = gallery || {}

$.each(gallery, function() {
  files.push(
  {
    name: this.title,
    size: this.id,
    type: "image/jpg",
    file: "/uploads/gallery/"+this.pic,
    url: this.pic,
  }
  );
});

$('#filer_input_edit').filer({
  showThumbs: true,
  addMore: true,
  allowDuplicates: false,
  templates: {
    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
    item: '<li class="jFiler-item" data-id="{{fi-size}}" data-image="{{fi-url}}" data-title="{{fi-name}}">\
    <div class="jFiler-item-container">\
    <div class="jFiler-item-inner">\
    <div class="jFiler-item-thumb">\
    <div class="jFiler-item-status"></div>\
    <div class="jFiler-item-thumb-overlay">\
    <div class="jFiler-item-info">\
    <div style="display:table-cell;vertical-align: middle;">\
    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    <span class="jFiler-item-others">{{fi-size2}}</span>\
    </div>\
    </div>\
    </div>\
    {{fi-image}}\
    </div>\
    <div class="jFiler-item-assets jFiler-row">\
    <ul class="list-inline pull-left">\
    <li><input placeholder="Título da imagem" id="title" name="titleimg[]" type="text" class="title" value="{{fi-name}}"></li>\
    </ul>\
    <ul class="list-inline pull-right">\
    <li><a class="jFiler-item-trash-action"><i class="material-icons">delete</i></a></li>\
    </ul>\
    </div>\
    </div>\
    </div>\
    </li>',
    itemAppend: '<li class="jFiler-item" data-id="{{fi-size}}" data-image="{{fi-url}}" data-title="{{fi-name}}">\
    <div class="jFiler-item-container">\
    <div class="jFiler-item-inner">\
    <div class="jFiler-item-thumb">\
    <div class="jFiler-item-status"></div>\
    <div class="jFiler-item-thumb-overlay">\
    <div class="jFiler-item-info">\
    <div style="display:table-cell;vertical-align: middle;">\
    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    <span class="jFiler-item-others">{{fi-size2}}</span>\
    </div>\
    </div>\
    </div>\
    {{fi-image}}\
    </div>\
    <div class="jFiler-item-assets jFiler-row">\
    <ul class="list-inline pull-left">\
    <li><input placeholder="Título da imagem" id="title" name="titleimg[]" type="text" class="title" value="{{fi-name}}"></li>\
    </ul>\
    <ul class="list-inline pull-right">\
    <li><a class="jFiler-item-trash-action"><i class="material-icons">delete</i></a></li>\
    </ul>\
    </div>\
    </div>\
    </div>\
    </li>',
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: false,
    canvasImage: true,
    removeConfirmation: true,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.jFiler-item-trash-action'
    }
  },
  captions: {
    button: "Adicionar",
    feedback: "Escolha os arquivos para adicionar",
    feedback2: "arquivos selecionados",
    drop: "Drop file here to Upload",
    removeConfirmation: "Você tem certeza que deseja excluir esse arquivo?",
    errors: {
      filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
      filesType: "Only Images are allowed to be uploaded.",
      filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
      filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
    }
  },
  files: files,
  options: {object:null}
});

function getOldFiles() {
  var old_files = [];
  $('.jFiler-item').each(function(){
    old_files.push({
      id: $(this).data('id'),
      title: $(this).find('input').val(),
      image: $(this).data('image')
    });
  });
  $('#old_files').val(JSON.stringify(old_files));
  console.log(JSON.stringify(old_files));
}

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    getOldFiles();
  });    
});

var observerConfig = {
  attributes: true, 
  childList: true, 
  characterData: true 
};

var targetNode = $('.jFiler-items-list')[0];
if(targetNode) {
  observer.observe(targetNode, observerConfig);
}

$(".title").on( "change", function() {
  getOldFiles();
});

$(document).ready(function(){
  getOldFiles();
});