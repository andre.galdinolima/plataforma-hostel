$('.dropdown-button').dropdown({
	inDuration: 225,
	outDuration: 225,
	constrain_width: false,
	hover: true,
	belowOrigin: true
});

$('.collapsible').collapsible();

$('.button-collapse').sideNav({
	edge: 'right'
});

$(document).ready(function(){
	$(this).scrollTop(0);
});

$(window).scroll(function() {
	if ($(document).scrollTop() > 50) {
		$('.navbar-menu').addClass('fixed-nav');
		$('.brand-logo').addClass('smaller');
		$('.nav-wrapper > ul').addClass('no-padding');
		$('.navbar-top').addClass('fixed-margin');
	} else {
		$('nav').removeClass('fixed-nav');
		$('.brand-logo').removeClass('smaller');
		$('.nav-wrapper > ul').removeClass('no-padding');
		$('.navbar-top').removeClass('fixed-margin');
	}
});