	<?php
Route::model('post','App\Post');
Route::model('event','App\Event');
Route::model('category','App\Category');
Route::model('type','App\Type');
Route::model('accommodation','App\Accommodation');
Route::model('service','App\Service');
Route::model('staff','App\Staff');
Route::model('user','App\User');
Route::model('term','App\Term');
Route::model('icon','App\Icon');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', ['as'=>'index', 'uses'=>'FrontendController@index']);
Route::get('/terms', ['as'=>'terms', 'uses'=>'FrontendController@terms']);
Route::get('attractions', ['as'=>'blog', 'uses'=>'FrontendController@blog']);
Route::get('attractions/category/{slug}', ['as'=>'blog.category', 'uses'=>'FrontendController@blogCategory']);
Route::get('attractions/post/{slug}', ['as'=>'blog.show', 'uses'=>'FrontendController@post']);

Route::get('news', ['as'=>'news', 'uses'=>'FrontendController@news']);

Route::get('galleries', ['as'=>'galleries', 'uses'=>'FrontendController@galleries']);

Route::get('event/{slug}', ['as'=>'events.show', 'uses'=>'FrontendController@eventSlug']);
Route::get('events', ['as'=>'events', 'uses'=>'FrontendController@events']);

Route::get('accommodation/{slug}', ['as'=>'room.accommodation', 'uses'=>'FrontendController@accommodation']);

Route::get('booking', ['as'=>'booking', 'uses'=>'FrontendController@booking']);

Route::get('about', ['as'=>'about', 'uses'=>'FrontendController@about']);

Route::get('contact', ['as'=>'contact', 'uses'=>'FrontendController@contact']);

Route::get('location', ['as'=>'location', 'uses'=>'FrontendController@location']);
Route::post('sendContact', ['as'=>'send.mail', 'uses'=>'FrontendController@postContact']);

//switch language. Default is english
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::group(['middleware'=>'SentinelGuest'],function(){
	Route::get('login',['as'=>'login','uses'=>'Auth\LoginController@getSignin']);
	Route::post('postSignin', ['as'=>'postSignin'  ,'uses'=>'Auth\LoginController@postSignin']);
	Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
	Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
	Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
	Route::post('password/reset', ['as' => 'password.request', 'uses' => 'Auth\ResetPasswordController@reset']);
});

Route::group(['middleware'=>'SentinelUser'],function(){
	Route::get('logout',['as'=>'logout','uses'=>'Auth\LoginController@getLogout']);

	Route::group(['prefix'=>'admin','as'=>'dash.'],function(){
		Route::get('/',['as'=>'index','uses'=>'DashboardController@index']);
		
		Route::group(['prefix'=>'posts','as'=>'post.'],function(){
			Route::get('/',['as'=>'index','uses'=>'PostController@index']);	
			Route::get('create',['as'=>'create','uses'=>'PostController@create']);
			Route::post('store',['as'=>'store','uses'=>'PostController@store']);
			Route::get('show/{post}',['as'=>'show','uses'=>'PostController@show']);	
			Route::get('edit/{post}',['as'=>'edit','uses'=>'PostController@edit']);	
			Route::put('update/{post}',['as'=>'update','uses'=>'PostController@update']);
			Route::get('delete/{post}',['as'=>'delete','uses'=>'PostController@destroy']);	
		});

		Route::group(['prefix'=>'categories','as'=>'category.'],function(){
			Route::get('/',['as'=>'index','uses'=>'CategoryController@index']);	
			Route::get('create',['as'=>'create','uses'=>'CategoryController@create']);
			Route::post('store',['as'=>'store','uses'=>'CategoryController@store']);
			Route::get('show/{category}',['as'=>'show','uses'=>'CategoryController@show']);	
			Route::get('edit/{category}',['as'=>'edit','uses'=>'CategoryController@edit']);	
			Route::put('update/{category}',['as'=>'update','uses'=>'CategoryController@update']);
			Route::get('delete/{category}',['as'=>'delete','uses'=>'CategoryController@destroy']);	
		});

		Route::group(['prefix'=>'events','as'=>'event.'],function(){
			Route::get('/',['as'=>'index','uses'=>'EventController@index']);	
			Route::get('create',['as'=>'create','uses'=>'EventController@create']);
			Route::post('store',['as'=>'store','uses'=>'EventController@store']);
			Route::get('show/{event}',['as'=>'show','uses'=>'EventController@show']);	
			Route::get('edit/{event}',['as'=>'edit','uses'=>'EventController@edit']);	
			Route::put('update/{event}',['as'=>'update','uses'=>'EventController@update']);
			Route::get('delete/{event}',['as'=>'delete','uses'=>'EventController@destroy']);	
		});

		Route::group(['prefix'=>'galleries','as'=>'gallery.'],function(){
			Route::get('/',['as'=>'index','uses'=>'GalleryController@index']);	
			Route::get('create',['as'=>'create','uses'=>'GalleryController@create']);
			Route::post('store',['as'=>'store','uses'=>'GalleryController@store']);
			Route::get('show/{gallery}',['as'=>'show','uses'=>'GalleryController@show']);	
			Route::get('edit/{gallery}',['as'=>'edit','uses'=>'GalleryController@edit']);	
			Route::put('update/{gallery}',['as'=>'update','uses'=>'GalleryController@update']);
			Route::get('delete/{gallery}',['as'=>'delete','uses'=>'GalleryController@destroy']);	
		});


		Route::group(['prefix'=>'accommodations','as'=>'accommodation.'],function(){
			Route::get('/',['as'=>'index','uses'=>'AccommodationController@index']);	
			Route::get('create',['as'=>'create','uses'=>'AccommodationController@create']);
			Route::post('store',['as'=>'store','uses'=>'AccommodationController@store']);
			Route::get('show/{accommodation}',['as'=>'show','uses'=>'AccommodationController@show']);	
			Route::get('edit/{accommodation}',['as'=>'edit','uses'=>'AccommodationController@edit']);	
			Route::put('update/{accommodation}',['as'=>'update','uses'=>'AccommodationController@update']);
			Route::get('delete/{accommodation}',['as'=>'delete','uses'=>'AccommodationController@destroy']);	
		});

		Route::group(['prefix'=>'typeaccommodations','as'=>'type_accommodation.'],function(){
			Route::get('/',['as'=>'index','uses'=>'TypeController@index']);	
			Route::get('create',['as'=>'create','uses'=>'TypeController@create']);
			Route::post('store',['as'=>'store','uses'=>'TypeController@store']);
			Route::get('show/{type}',['as'=>'show','uses'=>'TypeController@show']);	
			Route::get('edit/{type}',['as'=>'edit','uses'=>'TypeController@edit']);	
			Route::put('update/{type}',['as'=>'update','uses'=>'TypeController@update']);
			Route::get('delete/{type}',['as'=>'delete','uses'=>'TypeController@destroy']);	
		});

		Route::group(['prefix'=>'services','as'=>'service.'],function(){
			Route::get('/',['as'=>'index','uses'=>'ServiceController@index']);	
			Route::get('create',['as'=>'create','uses'=>'ServiceController@create']);
			Route::post('store',['as'=>'store','uses'=>'ServiceController@store']);
			Route::get('show/{service}',['as'=>'show','uses'=>'ServiceController@show']);	
			Route::get('edit/{service}',['as'=>'edit','uses'=>'ServiceController@edit']);	
			Route::put('update/{service}',['as'=>'update','uses'=>'ServiceController@update']);
			Route::get('delete/{service}',['as'=>'delete','uses'=>'ServiceController@destroy']);	
		});

		Route::group(['prefix'=>'staffs','as'=>'staff.'],function(){
			Route::get('/',['as'=>'index','uses'=>'StaffController@index']);	
			Route::get('create',['as'=>'create','uses'=>'StaffController@create']);
			Route::post('store',['as'=>'store','uses'=>'StaffController@store']);
			Route::get('show/{staff}',['as'=>'show','uses'=>'StaffController@show']);	
			Route::get('edit/{staff}',['as'=>'edit','uses'=>'StaffController@edit']);	
			Route::put('update/{staff}',['as'=>'update','uses'=>'StaffController@update']);
			Route::get('delete/{staff}',['as'=>'delete','uses'=>'StaffController@destroy']);	
		});

		Route::group(['prefix'=>'users','as'=>'user.'],function(){
			Route::get('/',['as'=>'index','uses'=>'UserController@index']);	
			Route::get('create',['as'=>'create','uses'=>'UserController@create']);
			Route::post('store',['as'=>'store','uses'=>'UserController@store']);
			//Route::get('show/{user}',['as'=>'show','uses'=>'UserController@show']);	
			Route::get('edit/{user}',['as'=>'edit','uses'=>'UserController@edit']);	
			Route::put('update/{user}',['as'=>'update','uses'=>'UserController@update']);
			Route::get('delete/{user}',['as'=>'delete','uses'=>'UserController@destroy']);	
			//Route::get('restore/{id}', array('as' => 'restore', 'uses' => 'UserController@restore'));
		});

		//role
		Route::group(['prefix'=>'roles','as'=>'role.'], function(){
			Route::get('/', array('as' => 'index', 'uses' => 'RoleController@index'));
			Route::get('create', array('as' => 'create', 'uses' => 'RoleController@create'));
			Route::post('store', array('as' => 'store', 'uses' => 'RoleController@store'));
			Route::get('edit/{id}', array('as' => 'edit', 'uses' => 'RoleController@edit'));
			Route::put('update/{id}', array('as' => 'update', 'uses' => 'RoleController@update'));
			Route::get('delete/{id}', array('as' => 'delete', 'uses' => 'RoleController@destroy'));
		});

		Route::group(['prefix'=>'terms','as'=>'term.'],function(){
			Route::get('/',['as'=>'index','uses'=>'TermController@index']);	
			Route::get('create',['as'=>'create','uses'=>'TermController@create']);
			Route::post('store',['as'=>'store','uses'=>'TermController@store']);
			Route::get('show/{term}',['as'=>'show','uses'=>'TermController@show']);	
			Route::get('edit/{term}',['as'=>'edit','uses'=>'TermController@edit']);	
			Route::put('update/{term}',['as'=>'update','uses'=>'TermController@update']);
			Route::get('delete/{term}',['as'=>'delete','uses'=>'TermController@destroy']);	
		});

		Route::group(['prefix'=>'icons','as'=>'icon.'],function(){
			Route::get('/',['as'=>'index','uses'=>'IconController@index']);	
			Route::get('create',['as'=>'create','uses'=>'IconController@create']);
			Route::post('store',['as'=>'store','uses'=>'IconController@store']);
			Route::get('show/{icon}',['as'=>'show','uses'=>'IconController@show']);	
			Route::get('edit/{icon}',['as'=>'edit','uses'=>'IconController@edit']);	
			Route::put('update/{icon}',['as'=>'update','uses'=>'IconController@update']);
			Route::get('delete/{icon}',['as'=>'delete','uses'=>'IconController@destroy']);	
		});


		Route::get('config',['as'=>'config','uses'=>'ConfigurationController@show']);
		Route::post('config/store',['as'=>'config.store','uses'=>'ConfigurationController@store']);

	});
});